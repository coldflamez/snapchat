package android.abujaberj.snapchat;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class AddFriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        AddFriendFragment addFriendFragment = new AddFriendFragment ();
        getSupportFragmentManager().beginTransaction().add(R.id.friendContainer, addFriendFragment).commit();
    }
}