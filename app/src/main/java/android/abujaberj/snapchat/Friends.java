package android.abujaberj.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Friends extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);


        FriendsFragment friendsFragment = new FriendsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.loginContainer, friendsFragment).commit();
    }
}
