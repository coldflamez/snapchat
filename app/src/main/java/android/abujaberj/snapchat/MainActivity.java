package android.abujaberj.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "C601441E-123A-39AA-FF2B-1F3843CE8100";
    public static String SECRET_KEY = "1BB93719-721B-5C38-FF7A-ED6AC2E30500";
    public static String VERSION = "v1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        Log.e("main", Backendless.UserService.loggedInUser().toString());
        if (Backendless.UserService.loggedInUser() == "") {
            Log.e("Main", "Not logged in");
            LoginFrag2 loginFrag2 = new LoginFrag2();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginFrag2).commit();

        } else {
            Log.e("Main", "logged in");
            MainMenuFragment mainMenuFragment = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenuFragment).commit();
        }
    }
}
